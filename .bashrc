#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Alias for replace commands by others 

alias ls='exa --color=auto --icons'
alias la='exa --color=auto --icons -la'
alias cat='bat --color=auto'
alias grep='grep --color=auto'
alias nv='nvim'
alias i3conf='nv /home/atipique_/.config/i3/config'
alias polyconf='nv /home/atipique_/.config/polybar/config.ini'
alias py='python3'

# Custom prompt text
PS1=" ~  "

export EDITOR=nv 
