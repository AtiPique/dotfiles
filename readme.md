﻿# DOTFILES ! 

These dotfiles are made for my computer and some (not very important)
features may not work on your machine without modification.
![rice preview](./PREVIEW.png)

## Description 

These are dotfiles for my i3 rice on Void Linux with a tutorial for
install it from a minimal void linux install. If you like it feel
free to give me a star on this repo :) !

### Install 

This rice uses these following packages :

| Package	       | Role		         	|
|----------------------|--------------------------------|
| xorg		       | Display Manager         	|
| i3-gaps	       | Windows Manger          	|
| lightdm              | GUI : desktop and Login 	|
| lightdm-gtk3-greeter | lightdm tool            	|
| elogind 	       | login tool		 	|
| kitty 	       | terminal		        |
| feh 		       | image viewer / background tool |
| bat		       | better cat	         	|
| exa                  | better ls               	|
| rofi		       | program launcher        	|
| polybar	       | the BAR                 	|
| neovim 	       | text/code editor	 	|
| git		       | git repo cloner         	|

## Pre-install on a new system

### I need to do some changes on the pre-install section.
### If i3 isn't installed on your system i let you find how on internet.
### And then go to the Installing the rice section.

>Theses commands are intended to be executed after a fresh install

* Update your sources
```shell
sudo xbps-install -Su
```

* Then install the basic packages
```shell
sudo xbps-install xorg i3-gaps lightdm lightdm-gtk3-greeter elogind git neovim firefox kitty
```

* Enable lightdm and elogind
```shell
sudo ln -s /etc/sv/elogind /var/service
sudo ln -s /etc/sv/lightdm /var/service
sudo ln -s /etv/sv/NetworkManager
sudo sv up NetworkManager
sudo sv up elogind
```

* Then reboot and test if i3 is working

## Installing the rice

* Clone the repo
```shell
git clone https://gitlab.com/AtiPique/dotfiles
```

* Moving config files
```shell 
cp -r dotfiles/.config ~/.config
cp -r dotfiles/.bashrc ~/.bashrc
cp -r dotfiles/JetBrainsMono /usr/share/fonts/
```

* Install tools 
```shell 
sudo xbps-install exa bat feh
```

* Then reload the i3 config file by using this shortcut
```shell
super + shift + e
```

## Shortcuts (i3)

| Shortcut		   | Action		            |
|--------------------------|--------------------------------|
| super + shift + a	   | Close window		    |
| super + d		   | Open rofi (app launcher)       |
| super + return	   | Open Kitty terminal	    |
| super + shift + f	   | floating mode		    |
| super + ctrl + {j,k,l,m} | move a floating window	    |
| super + f		   | fullscreen mode		    |
| super + r / escape	   | enter / quit resize mode	    |
| {j, k, l, m}		   | resize a window in resize mode |
| super + shift + r	   | reload i3 config file	    |
| super + shift + e	   | logout from i3 session 	    |
| super + {j, k, l, m}     | current window place           |
| super + v 		   | opens next windows verticaly   |
| super + h		   | opens next windows horizontaly |

## Version history

* 1.0 
	* First verison, change needed on polybar 
		* a script will be released soon for automate the dotfiles install

## License 

This project is licensed under the GPL3 License - read the LICENSE file for details
![license](./gplv3.png)


## Credit

* South Park Randy for the wallpaper.
